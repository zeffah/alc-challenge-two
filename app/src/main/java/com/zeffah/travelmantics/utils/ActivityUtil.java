package com.zeffah.travelmantics.utils;

import android.view.View;
import android.widget.ProgressBar;

public class ActivityUtil {

    public static void showLoadingIndicator(ProgressBar progressBar){
        if (progressBar != null && !progressBar.isShown()){
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    public static void hideLoadingIndicator(ProgressBar progressBar) {
        if (progressBar != null && progressBar.isShown()){
            progressBar.setVisibility(View.GONE);
        }
    }
}
