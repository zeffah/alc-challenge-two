package com.zeffah.travelmantics.utils;

public class ConstantsUtil {
    public static final String KEY_TRAVEL_DEAL = "Deal";
    public static final String TRAVEL_DEALS_NODE = "travel_deals";
    public static final int RC_SIGN_IN = 123;
    public static final String ADMINISTRATORS_NODE = "administrators";
    public static final int UPLOAD_IMAGE_REQUEST_CODE = 20;
    public static final String DEAL_PICTURES_STORAGE_NODE = "deals_pictures";
}
