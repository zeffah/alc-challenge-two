package com.zeffah.travelmantics.utils;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuth.AuthStateListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.zeffah.travelmantics.TravelDealActivity;
import com.zeffah.travelmantics.TravelDealsListActivity;
import com.zeffah.travelmantics.models.TravelDeal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FirebaseUtil {
    public static DatabaseReference databaseReference;
    public static FirebaseStorage firebaseStorage;
    public static StorageReference storageReference;
    public static ArrayList<TravelDeal> mTravelDeals;
    public static boolean isUserAdmin;

    private static FirebaseDatabase firebaseDatabase;
    private static FirebaseUtil firebaseUtil;
    private static FirebaseAuth firebaseAuth;
    private static AuthStateListener authStateListener;

    private FirebaseUtil() { }

    public static void openDatabaseReference(final Context mContext, String reference) {
        if (firebaseUtil == null) {
            firebaseUtil = new FirebaseUtil();
            firebaseDatabase = FirebaseDatabase.getInstance();
            firebaseAuth = FirebaseAuth.getInstance();
            authStateListener = new AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user == null) {
                        FirebaseUtil.signInUser(mContext);
                    } else {
                        String UID = firebaseAuth.getUid();
                        FirebaseUtil.checkIfUserIsAdmin(mContext, UID);
                    }
                }
            };

            connectToStorage();
        }
        mTravelDeals = new ArrayList<>();
        databaseReference = firebaseDatabase.getReference().child(reference);
    }

    private static void checkIfUserIsAdmin(final Context context, String uid) {
        FirebaseUtil.isUserAdmin = false;
        final DatabaseReference dbRef = firebaseDatabase
                .getReference()
                .child(ConstantsUtil.ADMINISTRATORS_NODE)
                .child(uid);

        final ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                FirebaseUtil.isUserAdmin = true;
                if (context instanceof TravelDealsListActivity) {
                    ((TravelDealsListActivity) context).toggleMenu();
                    Log.d("CONTEXT_CALLER", TravelDealsListActivity.class.getSimpleName());
                }

                if (context instanceof TravelDealActivity) {
                    ((TravelDealActivity) context).toggleMenu();
                    Log.d("CONTEXT_CALLER", TravelDealActivity.class.getSimpleName());
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        dbRef.addChildEventListener(childEventListener);

    }

    private static void signInUser(Context context) {
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build());
        // Create and launch sign-in intent
        ((AppCompatActivity) context).startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                ConstantsUtil.RC_SIGN_IN);
    }

    public static void attachAuthStateListener() {
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    public static void detachAuthStateListener() {
        firebaseAuth.removeAuthStateListener(authStateListener);
    }

    public static void logoutUser(Context context) {
        AuthUI.getInstance()
                .signOut(context)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        FirebaseUtil.attachAuthStateListener();
                    }
                });
    }

    private static void connectToStorage() {
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference().child(ConstantsUtil.DEAL_PICTURES_STORAGE_NODE);
    }
}
