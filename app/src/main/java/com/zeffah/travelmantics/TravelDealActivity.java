package com.zeffah.travelmantics;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.zeffah.travelmantics.models.TravelDeal;
import com.zeffah.travelmantics.utils.ActivityUtil;
import com.zeffah.travelmantics.utils.ConstantsUtil;
import com.zeffah.travelmantics.utils.FirebaseUtil;

import org.w3c.dom.Text;

public class TravelDealActivity extends AppCompatActivity implements View.OnClickListener {

    private DatabaseReference databaseReference;
    private TravelDeal travelDeal;

    private EditText edtTitle, edtPrice, edtDescription;
    private Button btnUploadImage;
    private ImageView imgDealPicture;
    private ProgressBar loadingIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel_deal);

        FirebaseUtil.openDatabaseReference(TravelDealActivity.this, ConstantsUtil.TRAVEL_DEALS_NODE);
        databaseReference = FirebaseUtil.databaseReference;

        loadingIndicator = findViewById(R.id.loading_indicator);
        edtDescription = findViewById(R.id.edtDescription);
        edtPrice = findViewById(R.id.edtPrice);
        edtTitle = findViewById(R.id.edtTitle);
        imgDealPicture = findViewById(R.id.img_deal_picture);
        btnUploadImage = findViewById(R.id.btn_add_image);
        btnUploadImage.setOnClickListener(this);

        Intent intent = getIntent();

        TravelDeal travelDeal = (TravelDeal) intent.getSerializableExtra(ConstantsUtil.KEY_TRAVEL_DEAL);
        if (travelDeal == null) {
            travelDeal = new TravelDeal();
        }
        this.travelDeal = travelDeal;
        edtDescription.setText(this.travelDeal.getDescription());
        edtPrice.setText(this.travelDeal.getPrice());
        edtTitle.setText(this.travelDeal.getTitle());
        loadImageToView(this.travelDeal.getImageUrl());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_travel_deal, menu);
        MenuItem deleteDeal = menu.findItem(R.id.action_delete_deal);
        MenuItem saveDeal = menu.findItem(R.id.action_save_deal);

        if (FirebaseUtil.isUserAdmin){
            deleteDeal.setVisible(true);
            saveDeal.setVisible(true);
            enableEditText(true);
        }else {
            deleteDeal.setVisible(false);
            saveDeal.setVisible(false);
            enableEditText(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_save_deal: {
                saveDeal();
                return true;
            }
            case R.id.action_delete_deal: {
                deleteDeal();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveDeal() {
        String title = edtTitle.getText().toString().trim();
        String price = edtPrice.getText().toString().trim();
        String description = edtDescription.getText().toString().trim();
        this.travelDeal.setTitle(title);
        this.travelDeal.setDescription(description);
        this.travelDeal.setPrice(price);
        this.travelDeal.setImageUrl(this.travelDeal.getImageUrl());
        if (this.travelDeal.getId() == null) {
            databaseReference.push().setValue(this.travelDeal, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    Toast.makeText(TravelDealActivity.this, "Deal Created Successfully", Toast.LENGTH_SHORT).show();
                    clean();
                    backToList();
                }
            });
        } else {
            databaseReference.child(this.travelDeal.getId()).setValue(this.travelDeal, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    Toast.makeText(TravelDealActivity.this, "Deal Updated Successfully", Toast.LENGTH_SHORT).show();
                    clean();
                    backToList();
                }
            });
        }
    }

    private void deleteDeal() {
        if (travelDeal.getId() == null) {
            Toast.makeText(this, "The deal you trying to deal is not created yet.", Toast.LENGTH_SHORT).show();
            return;
        }
        databaseReference.child(travelDeal.getId()).removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if (databaseError == null) {
                    Toast.makeText(TravelDealActivity.this, "Deal Removed Successfully", Toast.LENGTH_SHORT).show();
                    backToList();
                }
            }
        });

        if (!TextUtils.isEmpty(travelDeal.getImageName())){
            StorageReference picRef = FirebaseUtil.firebaseStorage.getReference().child(travelDeal.getImageName());
            picRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d("DEAL_DELETE", "deal deleted");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d("DEAL_DELETE", "deal failed: "+e.getLocalizedMessage());
                }
            });
        }
    }

    private void backToList() {
        Intent intent = new Intent(this, TravelDealsListActivity.class);
        startActivity(intent);
    }

    private void clean() {
        edtTitle.setText("");
        edtPrice.setText("");
        edtDescription.setText("");
    }

    public void toggleMenu() {
        invalidateOptionsMenu();
    }

    private void enableEditText(boolean isAdmin){
        edtTitle.setEnabled(isAdmin);
        edtPrice.setEnabled(isAdmin);
        edtDescription.setEnabled(isAdmin);
    }

    @Override
    public void onClick(View v) {
        if (v == btnUploadImage){
            uploadImage();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode ==RESULT_OK){
            if (requestCode == ConstantsUtil.UPLOAD_IMAGE_REQUEST_CODE){
                if (data != null) {
                    Uri imageUri = data.getData();
                    if (imageUri != null && imageUri.getLastPathSegment() != null) {
                        ActivityUtil.showLoadingIndicator(loadingIndicator);
                        final StorageReference ref = FirebaseUtil.storageReference.child(imageUri.getLastPathSegment());

                        ref.putFile(imageUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                            @Override
                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> taskSnapshot) {
                                if (!taskSnapshot.isSuccessful()) {
                                    return null;
                                }
                                String imageName = taskSnapshot.getResult().getMetadata().getPath();
                                travelDeal.setImageName(imageName);
                                return ref.getDownloadUrl();
                            }
                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {
                                    Uri downloadUri = task.getResult();
                                    if (downloadUri != null) {
                                        travelDeal.setImageUrl(downloadUri.toString());
                                        loadImageToView(travelDeal.getImageUrl());
                                    }
                                }
                                ActivityUtil.hideLoadingIndicator(loadingIndicator);
                            }
                        });
                    }
                }
            }
        }
    }

    private void loadImageToView(String url) {
        if (!TextUtils.isEmpty(url)){
            int width = Resources.getSystem().getDisplayMetrics().widthPixels;
            int height = width *2/3;
            Picasso.get()
                    .load(url)
                    .resize(width, height)
                    .centerCrop(Gravity.CENTER)
                    .into(imgDealPicture);
        }
    }

    private void uploadImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.upload_picture)),
                ConstantsUtil.UPLOAD_IMAGE_REQUEST_CODE);
    }
}
