package com.zeffah.travelmantics.adapters;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.squareup.picasso.Picasso;
import com.zeffah.travelmantics.R;
import com.zeffah.travelmantics.TravelDealActivity;
import com.zeffah.travelmantics.models.TravelDeal;
import com.zeffah.travelmantics.utils.ActivityUtil;
import com.zeffah.travelmantics.utils.ConstantsUtil;
import com.zeffah.travelmantics.utils.FirebaseUtil;

import java.util.ArrayList;

public class TravelDealsAdapter extends RecyclerView.Adapter<TravelDealsAdapter.DealsViewHolder> implements ChildEventListener {
    private ArrayList<TravelDeal> travelDeals;
    private ProgressBar progressBar;

    public TravelDealsAdapter(Context context, ProgressBar progressBar) {
        this.progressBar = progressBar;
        ActivityUtil.showLoadingIndicator(progressBar);
        FirebaseUtil.openDatabaseReference(context, ConstantsUtil.TRAVEL_DEALS_NODE);
        DatabaseReference databaseReference = FirebaseUtil.databaseReference;
        databaseReference.addChildEventListener(this);
        travelDeals = FirebaseUtil.mTravelDeals;
    }

    @NonNull
    @Override
    public DealsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View convertView = LayoutInflater.from(context)
                .inflate(R.layout.travel_destination_row, parent, false);
        return new DealsViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(@NonNull DealsViewHolder holder, int position) {
        TravelDeal travelDeal = travelDeals.get(position);
        if (travelDeal != null) {
            holder.bindData(travelDeal);
        }
    }

    @Override
    public int getItemCount() {
        return travelDeals.size();
    }

    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        TravelDeal travelDeal = dataSnapshot.getValue(TravelDeal.class);
        if (travelDeal != null) {
            travelDeal.setId(dataSnapshot.getKey());
            travelDeals.add(travelDeal);
            notifyItemInserted(travelDeals.size() - 1);
        }
        ActivityUtil.hideLoadingIndicator(progressBar);
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    public class DealsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvTitle, tvDescription, tvPrice;
        private ImageView imgAvatar;

        public DealsViewHolder(@NonNull View view) {
            super(view);
            tvTitle = view.findViewById(R.id.tv_title);
            tvDescription = view.findViewById(R.id.tv_description);
            tvPrice = view.findViewById(R.id.tv_price);
            imgAvatar = view.findViewById(R.id.img_deal_avatar);
            view.setOnClickListener(this);
        }

        public void bindData(TravelDeal travelDeal) {
            tvPrice.setText(String.valueOf(travelDeal.getPrice()));
            tvTitle.setText(travelDeal.getTitle());
            tvDescription.setText(travelDeal.getDescription());
            loadImage(travelDeal.getImageUrl());
        }

        private void loadImage(String url){
            if (TextUtils.isEmpty(url)){
                return;
            }
            Picasso.get()
                    .load(url)
                    .resize(200, 200)
                    .centerCrop(Gravity.CENTER)
                    .into(imgAvatar);
        }

        @Override
        public void onClick(View v) {
            boolean isAdmin = FirebaseUtil.isUserAdmin;
            if (!isAdmin){
                return;
            }
            TravelDeal travelDeal = travelDeals.get(getAdapterPosition());
            Intent intent = new Intent(v.getContext(), TravelDealActivity.class);
            intent.putExtra(ConstantsUtil.KEY_TRAVEL_DEAL, travelDeal);
            (v.getContext()).startActivity(intent);
        }
    }
}
