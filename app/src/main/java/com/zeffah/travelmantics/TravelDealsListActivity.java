package com.zeffah.travelmantics;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zeffah.travelmantics.adapters.TravelDealsAdapter;
import com.zeffah.travelmantics.utils.FirebaseUtil;

public class TravelDealsListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressBar loadingIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel_destination);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.travel_deals_menu, menu);
        MenuItem insertDeal = menu.findItem(R.id.action_insert_deal);
        if (FirebaseUtil.isUserAdmin){
            insertDeal.setVisible(true);
        }else {
            insertDeal.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemId = item.getItemId();
        switch (menuItemId){
            case R.id.action_insert_deal: {
                Intent insertDealIntent = new Intent(this, TravelDealActivity.class);
                startActivity(insertDealIntent);
                return true;
            }
            case R.id.action_logout: {
                FirebaseUtil.logoutUser(this);
                FirebaseUtil.detachAuthStateListener();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseUtil.detachAuthStateListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        final TravelDealsAdapter travelDealsAdapter = new TravelDealsAdapter(TravelDealsListActivity.this, loadingIndicator);
        recyclerView.setAdapter(travelDealsAdapter);
        FirebaseUtil.attachAuthStateListener();
    }

    private void init(){
        recyclerView = findViewById(R.id.lst_travel_deals);
        loadingIndicator = findViewById(R.id.loading_indicator);
                LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL,
                false);
        recyclerView.setLayoutManager(layoutManager);
    }

    public void toggleMenu(){
        invalidateOptionsMenu();
    }
}
